# VUR helper

Helper for Void User Repositories.

## Installation

### Install dependencies
```
sudo xbps-install git
```

### Initialize `vh`
```
git clone https://codeberg.org/eloitor/VUR-helper
cd VUR-helper
sudo make install # optionally you can run it locally as `./vh` 
vh # Add your void-linux/void-packages fork
```

## Update templates

```
vh --update
```

## Add more user repositories

TODO

## To build and install a template
```
vh ytfzf
```

## To edit a template
```
vh --edit ytfzf
```

## You can access the git repository of your VUR to commit and push your changes:
```
vh -c xbump ytfzf
vh -c git push
```

## You can access the xbps-src tool
```
vh -c xbps-src clean
```

## You can add pending PR to your VUR easly
```
mvur -c git checkout origin pull/36504/head:threejs-sage
mvur -c git checkout threejs-sage
mvur -g add threejs-sage
mvur -g commit -m "Added threejs-sage"
mvur -g push
```

## Usage info
```
mvur --help
```

To update `mvur-git` you have to manually remove the old one before runing `./mvur mvur-git`
