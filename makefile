PROG=vh
PREFIX=/usr/local
BINDIR=${PREFIX}/bin

install:
	install -m 755 ${PROG} ${BINDIR}

uninstall:
	rm -f ${BINDIR}/${PROG}

.PHONY: install
